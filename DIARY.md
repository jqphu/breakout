# Things I've learnt aka Diary

## 2018-06-17
* Working on the bomb POWERUP effect
* Made it destroy a 3x3 square around itself
    * Performance could be improved (just looping through all the boxes isn't great)
* Added a fire trail particle effect when the powerup is on 
* Added the ball turn dark black when it has the bomb powerup
* Need to implement an explosion (particles) when impacted


## 2018-06-16
**** BACK AT IT. 
* Quickly did post-processing implementation (will go through tutorial again after to understand). Want to get quickly to powerups!
* Issue with multi storage only allowing 1 sample instead of 8 (weird)
* POWERUPPPPPS
* Soon implement a bomb powerupp that can destroy solid blocks
* Double damage powerup that can kill 2 blocks 
* Save/Buy powerups? Use them with a hotkey!
* Double powerup where effects are double (either last twice as long or twice as fast)

* Attempt to create a bomb POWERUP
* Should destroy a 3x3 radius from impact (benefit is it should be used in conjunction)
* Add particle effects following the ball (fire)


## 2018-05-29
* Collisions could do with some work
* Particles are nice -> Consider a bomb mode
    * Bomb Powerup
        * Turns colour of ball to red
        * Fire trail
        * Explosion particle effect on contact
* Game is starting to come together!

## 2018-05-28
* Getting using to coding convention in snake case when have been using camel case at work
* kConstantVariable is also hard to get used to
* Game is starting to come together and actually look nice!

## 2018-05-27
* Lots of copy and pasting but lots of understanding
* Spent 1 hour figuring out a small issue of me trying to be smart.
* VALIDATE YOUR RESULTS EACH STEP and ensure things are working
* Need to learn some OpenGL debugging techniques

## 2018-05-26
* Finished the getting started tutorial! Time to build a game :)
* STATIC LINKING - linking files by directly including them in your project. Seems good
* Singleton, put the constructor private so the object can NEVER be created but rather the functions can be used... interesting
* -I<path> flag is together e.g. "-Iinclude" interesting...
* INSTALLING ERRORS CLASH, issue was using GLEW now not GLAD... :(

## 2018-05-24
* I think the idea is anything you currently bind `glBindBuffer` and `glBindData` is what you are working on now, that is you bind the buffer and the data then extra commands such as `glVertixAttribPointer` to describe the data and `glEnableVertexAttribArray` work on whatever is bound. You can unbind then by simply calling `glBindVertexArray(0)` but it may not be needed if you immediately bind something else after.
* Once you setup your attribute objects simply call `glBindVertexArray(VAO)` and now the object is all set up again ready to be drawn
* const AFTER member function declaration means IT WILL NOT MODIFY ANY MEMBER VARIABLES

## 2018-05-23
* Using the command line for everything makes things difficult
* BASH ON UBUNTU ON WINDOWS TO RUN A PROGRAM BUT VIEW IT IN WINDOWS <- pain


