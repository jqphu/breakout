# Breakout
My attempt at making the classic Breakout game using OpenGL following [learnGL](https://learnopengl.com/)
	
## Requirements
* export DISPLAY=:0.0
    * Set the display to our windows machine

* use VcXsrv Server
* OpenGL
* GLEW (GLAD for getting_started branch)
* SOIL
