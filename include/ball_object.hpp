#ifndef BREAKOUT_INCLUDE_BALL_OBJECT_
#define BREAKOUT_INCLUDE_BALL_OBJECT_

#include "game_object.hpp"

// Make the base class public to access all the members
class BallObject : public GameObject
{
    public:
        // Ball state
        GLfloat radius;
        GLboolean stuck; // For the start of the game it is stuck on the paddle
        GLboolean sticky, pass_through, bomb;

        BallObject();
        BallObject(glm::vec2 pos, GLfloat radius, glm::vec2 velocity, Texture2D sprite);

        glm::vec2 Move(GLfloat dt, GLuint window_width);
        void Reset(glm::vec2 position, glm::vec2 velocity);
};

#endif
