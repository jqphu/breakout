#ifndef SHADER_H
#define SHADER_H

#include <GL/glew.h>
#include "glm/glm.hpp"
#include "glm/gtc/type_ptr.hpp"


#include <string>
#include <fstream>
#include <sstream>
#include <iostream>

class Shader
{
    public:
        // Program ID
        unsigned int id;

        // Empty constructor
        Shader() {};

        // Use/Activate the shader
        Shader &Use();

        // Compiler the shader
        void Compile(const GLchar* vertex_path, const GLchar* fragment_path, const GLchar* geometry_source = nullptr);

        // Utility unfirom function
        void    SetFloat    (const GLchar *name, GLfloat value, GLboolean useShader = false);
        void    SetInteger  (const GLchar *name, GLint value, GLboolean useShader = false);
        void    SetVector2f (const GLchar *name, GLfloat x, GLfloat y, GLboolean useShader = false);
        void    SetVector2f (const GLchar *name, const glm::vec2 &value, GLboolean useShader = false);
        void    SetVector3f (const GLchar *name, GLfloat x, GLfloat y, GLfloat z, GLboolean useShader = false);
        void    SetVector3f (const GLchar *name, const glm::vec3 &value, GLboolean useShader = false);
        void    SetVector4f (const GLchar *name, GLfloat x, GLfloat y, GLfloat z, GLfloat w, GLboolean useShader = false);
        void    SetVector4f (const GLchar *name, const glm::vec4 &value, GLboolean useShader = false);
        void    SetMatrix4  (const GLchar *name, const glm::mat4 &matrix, GLboolean useShader = false);

    private:
        void CheckCompileErrors(GLuint object, std::string type);
};

#endif
