#ifndef BREAKOUT_INCLUDE_PARTICLE_GENERATOR_H_
#define BREAKOUT_INCLUDE_PARTICLE_GENERATOR_H_

#include <GL/glew.h>
#include "glm/glm.hpp"

#include "shader.hpp"
#include "texture.hpp"
#include "game_object.hpp"

struct Particle
{
    glm::vec2 position, velocity;
    glm::vec4 color;
    GLfloat life; // How long until it should be destroyed

    Particle()
        : position(0.0f), velocity(0.0f), color(1.0f), life(0.0f) {}
};

/**
 * Container for a large number of particles
 * Controls spawning and killing particles (including rendering)
 */
class ParticleGenerator
{
    public:
        ParticleGenerator(Shader shader, Texture2D texture, GLuint amount, std::string colour = "random");

        // Update all the particles
        void Update(GLfloat dt, GameObject& object, GLuint new_particles, glm::vec2 offset = glm::vec2(0.0f, 0.0f));

        // Render all particles
        void Draw();

    private:
        // Particles
        std::vector<Particle> particles;
        GLuint amount;

        // Render shader
        Shader shader;
        Texture2D texture;
        GLuint VAO;
        std::string colour;

        // Initalise buffer and vertex attributes
        void Init();

        // Returns the first particle index thats not used (Life <= 0.0f) or 0 if no particles inactive
        GLuint FirstUnusedParticle();

        // Respawn particle
        void RespawnParticle(Particle& particle, GameObject& object, glm::vec2 offset = glm::vec2(0.0f, 0.0f));
};


#endif
