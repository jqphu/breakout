#ifndef BREAKOUT_INCLUDE_SPRITE_RENDERER_H
#define BREAKOUT_INCLUDE_SPRITE_RENDERER_H

#include <GL/glew.h>
#include "glm/glm.hpp"
#include "glm/gtc/matrix_transform.hpp"

#include "shader.hpp"
#include "texture.hpp"

class SpriteRenderer
{
    public:
        SpriteRenderer(Shader& shader);
        ~SpriteRenderer();

        /**
         * Function to draw the sprite
         * Default size is 10x10 with 0 rotation and all black
         */
        void DrawSprite(Texture2D& texture, glm::vec2 position,
                glm::vec2 size = glm::vec2(10,10), GLfloat rotate = 0.0f,
                glm::vec3 color = glm::vec3(1.0f));

    private:

        // Holds a shader object to draw the sprite!
        Shader shader;
        GLuint quad_VAO;

        void InitRenderData();
};

#endif
