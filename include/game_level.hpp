#ifndef BREAKOUT_INCLUDE_GAME_LEVEL_H_
#define BREAKOUT_INCLUDE_GAME_LEVEL_H_

#include <vector>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "game_object.hpp"
#include "sprite_renderer.hpp"
#include "resource_manager.hpp"

class GameLevel
{
    public:
        std::vector<GameObject> bricks;

        GameLevel(){}

        /**
         * Load the level from a textfile
         * Format:
         * 0 is no brick
         * 1 is a solid brick, cannot be destroyed
         * >1 is a destroyable brick (higher number jut differs by color
         * E.g
         * 1 1 1 1 1 1
         * 2 2 0 0 2 2
         * 3 3 4 4 3 3
         */
        void Load(const GLchar* file, GLuint level_width, GLuint level_height);

        // Render level
        void Draw(SpriteRenderer& renderer);

        // Check if the level is completed
        GLboolean IsCompleted();
    private:
        // Initalise the level from the tile data
        void init(std::vector<std::vector<GLuint>> tile_data, GLuint level_wdith, GLuint level_height);

};

#endif
