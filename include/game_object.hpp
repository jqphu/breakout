#ifndef BREAKOUT_INCLUDE_GAME_OBJECT_H_
#define BREAKOUT_INCLUDE_GAME_OBJECT_H_

#include <GL/glew.h>
#include "glm/glm.hpp"

#include "texture.hpp"
#include "sprite_renderer.hpp"

/**
 * Base class to represent the minimum requirements of anything
 * inside the game
 */
class GameObject
{
    public:
        // States of the object
        glm::vec2 position, size, velocity;
        glm::vec3 color;
        GLfloat rotation; // What angle of rotation it is
        GLboolean is_solid;
        GLboolean destroyed;

        // All the information regarding the texture of this object
        Texture2D sprite;

        // Constructors
        GameObject();
        // Overload incase they want to put in inputs
        GameObject(glm::vec2 pos, glm::vec2 size, Texture2D sprite, glm::vec3 color = glm::vec3(1.0f), glm::vec2 velocity = glm::vec2(0.0f, 0.0f));

        /**
         *
         * Draw the block
         * Virtual allows it to be binded at RUNTIME instead of compile time
         * i.e. if we have a pointerof this base class but points to the DERIVED class
         * and we run the VIRTUAL function, since it is bound at runtime it will run the DERIVED CALSSES FUNCTION
         * Otherwise, it would just run our regular function
         * This is important as if we have a base class pointer and point it to a derived class to draw the sprite we want
         * to draw the derived class not the base class!
         */
        virtual void Draw(SpriteRenderer& renderer);

};

#endif
