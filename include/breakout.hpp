#ifndef BREAKOUT_H
#define BREAKOUT_H

#include <iostream>
#include <cstdio>
#include <cmath>

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "shader.hpp"
#include "texture.hpp"

// GLFW function declerations
void key_callback(GLFWwindow* window, int key, int scancode, int action, int mode);




#endif
