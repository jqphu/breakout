/*******************************************************************
 ** This code is part of Breakout.
 **
 ** Breakout is free software: you can redistribute it and/or modify
 ** it under the terms of the CC BY 4.0 license as published by
 ** Creative Commons, either version 4 of the License, or (at your
 ** option) any later version.
 ******************************************************************/

#ifndef BREAKOUT_INCLUDE_TEXTURE_H_
#define BREAKOUT_INCLUDE_TEXTURE_H_


#include <GL/glew.h>

// Texture2D is able to store and configure a texture in OpenGL.
// It also hosts utility functions for easy management.
class Texture2D
{
    public:
        // Holds the ID of the texture object, used for all texture operations to reference to this particlar texture
        GLuint id;

        // Texture image dimensions
        GLuint width, height; // Width and height of loaded image in pixels

        // Texture Format
        GLuint internal_format; // Format of texture object
        GLuint image_format; // Format of loaded image

        // Texture configuration
        GLuint wrap_s; // Wrapping mode on S axis
        GLuint wrap_t; // Wrapping mode on T axis
        GLuint filter_min; // Filtering mode if texture pixels < screen pixels
        GLuint filter_max; // Filtering mode if texture pixels > screen pixels

        // Constructor (sets default texture modes)
        Texture2D();

        // Generates texture from image data
        void Generate(GLuint width, GLuint height, unsigned char* data);

        // Binds the texture as the current active GL_TEXTURE_2D texture object
        void Bind() const;
};

#endif
