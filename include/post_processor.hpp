/*******************************************************************
 * This section has been copy and pasted from learnopengl.
 * Will come back to it after!
 *****************************************************************
 */
#ifndef BREAKOUT_INCLUDE_POST_PROCESSOR_H
#define BREAKOUT_INCLUDE_POST_PROCESSOR_H

#include <GL/glew.h>
#include "glm/glm.hpp"

#include "shader.hpp"
#include "texture.hpp"
#include "sprite_renderer.hpp"


// PostProcessor hosts all PostProcessing effects for the Breakout
// Game. It renders the game on a textured quad after which one can
// enable specific effects by enabling either the Confuse, Chaos or
// Shake boolean.
// It is required to call BeginRender() before rendering the game
// and EndRender() after rendering the game for the class to work.
class PostProcessor
{
    public:
        // State
        Shader post_processing_shader;
        Texture2D texture;
        GLuint width, height;
        // Options
        GLboolean confuse, chaos, shake;
        // Constructor
        PostProcessor(Shader shader, GLuint width, GLuint height);
        // Prepares the postprocessor's framebuffer operations before rendering the game
        void BeginRender();
        // Should be called after rendering the game, so it stores all the rendered data into a texture object
        void EndRender();
        // Renders the PostProcessor texture quad (as a screen-encompassing large sprite)
        void Render(GLfloat time);
    private:
        // Render state
        GLuint MSFBO, FBO; // MSFBO = Multisampled FBO. FBO is regular, used for blitting MS color-buffer to texture
        GLuint RBO; // RBO is used for multisampled color buffer
        GLuint VAO;
        // Initialize quad for rendering postprocessing texture
        void initRenderData();
};

#endif
