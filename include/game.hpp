#ifndef BREAKOUT_INCLUDE_GAME_H
#define BREAKOUT_INCLUDE_GAME_H


#include <string>

#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include "game_level.hpp"
#include "ball_object.hpp"
#include "particle_generator.hpp"
#include "powerup.hpp"


// Represents the current state of the game
enum GameState
{
    kGameActive,
    kGameMenu,
    kGameWin
};

// Direction of a vector
enum Direction
{
    kUp,
    kRight,
    kDown,
    kLeft
};

// Collision, direction and difference vector
typedef std::tuple<GLboolean, Direction, glm::vec2> Collision;



class Game
{
    public:
        // Game state
        GameState state;
        GLboolean keys[1024];
        GLuint width, height;

        // Levels
        std::vector<GameLevel> levels;
        GLuint current_level;

        // Powerups
        std::vector<Powerup> powerups;

        Game(GLuint width, GLuint height);
        ~Game();

        // Initalise game state (load shaders/textures/levels)
        void Init();

        // Game llop
        void ProcessInput(GLfloat dt);
        void Update(GLfloat dt);
        void ExplodeAroundTile(glm::vec2 pos);
        void Render();
        void UpdateCollisions();
        void ResetLevel();
        void ResetPlayer();
        void ResetPowerup();
        void SpawnPowerups(GameObject& block);
        void UpdatePowerups(GLfloat dt);
};

// Helper functions
GLboolean IsCollision(GameObject& one, GameObject& two);
Collision IsCollision(BallObject& one, GameObject& two);
Direction VectorDirection(glm::vec2 target);
GLboolean ShouldSpawn(GLuint chance);
void ActivatePowerup(Powerup &powerup);
GLboolean IsOtherPowerupActive(std::vector<Powerup> &powerups, std::string type);

#endif

