#ifndef BREAKOUT_INCLUDE_POWER_UP_
#define BREAKOUT_INCLUDE_POWER_UP__

#include "game_object.hpp"

const glm::vec2 kSize(60, 20); // Rectangle powerup
const glm::vec2 kVelocity(0.0f, 150.0f); // Falling straight down


class Powerup : public GameObject
{
    public:
        // Powerup state
        std::string type;
        GLfloat duration;
        GLboolean activated;

        // Constructor
        Powerup(std::string type, glm::vec3 color, GLfloat duration,
                glm::vec2 position, Texture2D texture)
            : GameObject(position, kSize, texture, color, kVelocity),
            type(type), duration(duration), activated() {};
};



#endif

