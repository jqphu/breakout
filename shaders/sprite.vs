#version 330 core

// vec2 position (2D game), vec2 (texCoordinates)
layout (location = 0) in vec4 vertex;

// Goes to frag
out vec2 TexCoords;

// Only model and projection because we have 2D game
// Skip camera view, straight to clip-space
uniform mat4 model;
uniform mat4 projection;

void main()
{
    TexCoords = vertex.zw;

    // Again z is 0 as 2D
    gl_Position = projection * model * vec4(vertex.xy, 0.0, 1.0);
}
