# Changelog
All notable changes to this project will be documented in this file.

## [Unreleased]
### Added
- README
- CHANGELOG
- Basic OpenGL display running on Ubunutu using VcXsrv

### Changed
N/A

### Removed
N/A


## [0.0.1] - 2018-05-26
### Added
- README
- CHANGELOG
- Basic OpenGL display running on Ubunutu using VcXsrv
- Getting Started files for BREAKOUT

### Changed
N/A

### Removed
N/A

---
The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)


