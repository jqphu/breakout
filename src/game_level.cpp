#include "game_level.hpp"

#include <string>
#include <cassert>
#include <fstream>
#include <sstream>

const std::string AppendLevelPath(const GLchar* level);

void GameLevel::Load(const GLchar* file, GLuint level_width, GLuint level_height)
{
    // Remove all old data from the vector
    this->bricks.clear();

    // Load the new data
    GameLevel level;
    std::string full_file = AppendLevelPath(file);
    std::ifstream fstream(full_file.c_str());
    std::vector<std::vector<GLuint>> tile_data;

    // If you can open the file with no errors
    if(fstream)
    {
        std::string line;
        // Read evrey line
        while(std::getline(fstream, line))
        {
            std::istringstream sstream(line);
            std::vector<GLuint> row;
            GLuint tile_code;
            // Read each int
            while(sstream >> tile_code)
            {
                row.push_back(tile_code);
            }
            tile_data.push_back(row);
        }

        // If there was data, initalise the level!
        if(tile_data.size() > 0)
        {
            this->init(tile_data, level_width, level_height);
        }
    }
}

void GameLevel::init(std::vector<std::vector<GLuint>> tile_data, GLuint level_width, GLuint level_height)
{
    // Calculate dimensions
    GLuint height = tile_data.size();
    GLuint width = tile_data[0].size();

    // Find how big the blocks are for this level (fill the whole screen)
    GLfloat unit_width = level_width / static_cast<GLfloat>(width);
    // Truncated, i'm not sure why its truncated
    // TODO: Figure out why its truncating height and not width
    GLfloat unit_height = level_height/ height;

    // Initalise levels based on tile_data!
    for (GLuint y = 0; y < height; ++y)
    {
        for(GLuint x = 0; x < width; ++x)
        {
            // Check block type!
            // Solid unbreakable
            if (tile_data[y][x] == 1)
            {
                glm::vec2 pos (unit_width * x, unit_height * y);
                glm::vec2 size(unit_width, unit_height);
                GameObject obj(pos, size,
                        ResourceManager::GetTexture("block_solid"),
                        glm::vec3(0.8f, 0.8f, 0.7f) // color
                        );
                obj.is_solid = GL_TRUE;
                this->bricks.push_back(obj);
            }
            else if (tile_data[y][x] > 1)
            {
                assert(tile_data[y][x] < 8); // Ensure there hasn't been and error when reading input

                glm::vec3 color = glm::vec3(0.0f); // White!
                if(tile_data[y][x] == 2)
                {
                    color = glm::vec3(0.2f, 0.6f, 1.0f);
                }
                else if (tile_data[y][x] == 3)
                {
                    color = glm::vec3(0.0f, 0.7f, 0.0f);
                }
                else if (tile_data[y][x] == 4)
                {
                    color = glm::vec3(0.8f, 0.8f, 0.4f);
                }
                else if (tile_data[y][x] == 5)
                {
                    color = glm::vec3(1.0f, 0.5f, 0.0f);
                }
                else if (tile_data[y][x] == 6)
                {
                    color = glm::vec3(0.3f, 0.3f, 0.3f);
                }

                glm::vec2 pos (unit_width * x, unit_height * y);
                glm::vec2 size(unit_width, unit_height);
                GameObject obj(pos, size,
                        ResourceManager::GetTexture("block"),
                        color
                        );
                this->bricks.push_back(obj);

            }
        }
    }

}

void GameLevel::Draw(SpriteRenderer& renderer)
{
    for(GameObject &tile : this->bricks)
    {
        if(!tile.destroyed)
        {
            tile.Draw(renderer);
        }
    }
}

GLboolean GameLevel::IsCompleted()
{
    for(GameObject &tile : this->bricks)
    {
        // Still a possible tile to destory
        if(!tile.is_solid && !tile.destroyed)
        {
            return GL_FALSE;
        }
    }
    return GL_TRUE;
}


const std::string AppendLevelPath(const GLchar* level)
{
    return (std::string("/home/justin/Dropbox/other/Breakout/levels/") + std::string(level));
}

