#include "ball_object.hpp"

#include <cmath>

const GLfloat kDefaultBallRadius = 12.5f;

BallObject::BallObject()
    : GameObject(), radius(kDefaultBallRadius), stuck(true), sticky(false), pass_through(false)  { }

BallObject::BallObject(glm::vec2 pos, GLfloat radius, glm::vec2 velocity, Texture2D sprite)
    :  GameObject(pos, glm::vec2(radius * 2, radius * 2), sprite, glm::vec3(1.0f), velocity), radius(radius), stuck(true), sticky(false), pass_through(false) { }

glm::vec2 BallObject::Move(GLfloat dt, GLuint window_width)
{
    // If we are not stuck
    if(!this->stuck)
    {
        // Move the ball
        glm::vec2 next_pos = this->position + this->velocity * dt;

        // Check if we hit a wall (out of bounds)
        // If so, reverse velocity and set to correct position
        if(next_pos.x < 0.0f)
        {
            this->velocity.x = -this->velocity.x;
            this->position = glm::vec2(std::fabs(next_pos.x), next_pos.y);
        }
        else if (next_pos.x + this->size.x > window_width)
        {
            this->velocity.x = -this->velocity.x;
            this->position = glm::vec2(
                    window_width - this->size.x - (std::fabs(next_pos.x + this->size.x - window_width)),
                    next_pos.y
                    );
        }
        else
        {
            this->position = next_pos;
        }

        // Bounc off the roof
        if(next_pos.y < 0.0f )
        {
            this->velocity.y = -this->velocity.y;
            this->position.y = std::fabs(next_pos.y);
        }
    }
    return this->position;
}


void BallObject::Reset(glm::vec2 position, glm::vec2 velocity)
{
    this->stuck = true;
    this->position = position;
    this->velocity = velocity;

}
