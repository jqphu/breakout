#include "sprite_renderer.hpp"

SpriteRenderer::SpriteRenderer(Shader& shader)
{
    this->shader = shader;
    this->InitRenderData();
}

SpriteRenderer::~SpriteRenderer()
{
    glDeleteVertexArrays(1, &this->quad_VAO);
}

void SpriteRenderer::InitRenderData()
{
    // Setup VAO/VBO
    GLuint VBO;

    // Verticies for a square block!
    GLfloat vertices[] =
    {
        // Pos      // Tex
        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f,

        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f
    };

    const unsigned int length = 4;
    const unsigned int stride = length * sizeof(GLfloat);

    // Put the array object ID into our member variable
    glGenVertexArrays(1, &this->quad_VAO);
    glGenBuffers(1, &VBO);

    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);

    glBindVertexArray(this->quad_VAO);
    glEnableVertexAttribArray(0);

    /**
     * Each element is of length 4, type FLOAT, no need to normalise with no offset
     */
    glVertexAttribPointer(0, length, GL_FLOAT, GL_FALSE, stride, (GLvoid*)0);
    glBindBuffer(GL_ARRAY_BUFFER, 0);
    glBindVertexArray(0);
}

void SpriteRenderer::DrawSprite(Texture2D& texture, glm::vec2 position,
        glm::vec2 size, GLfloat rotate, glm::vec3 color)
{
    // Set up our sprite modle given the parameters
    this->shader.Use();

    // Read operations from bottom to top! (Matrix applied right to left)
    glm::mat4 model;

    // Move it to new position last
    model = glm::translate(model, glm::vec3(position, 0.0f));

    // Move position back to top left corner
    model = glm::translate(model, glm::vec3(0.5f * size.x, 0.5f * size.y, 0.0f));

    // Rotate about the z axis
    model = glm::rotate(model, rotate, glm::vec3(0.0f, 0.0f, 1.0f));

    // Since position is defined by top left, we need to move the position to the center of the quad
    // for rotation
    model = glm::translate(model, glm::vec3(-0.5f * size.x, -0.5f * size.y, 0.0f));

    // Scale
    model = glm::scale(model, glm::vec3(size, 1.0f));

    this->shader.SetMatrix4("model", model);
    this->shader.SetVector3f("sprite_color", color);

    // Again, 0 is activated by default, but we'll activate it anyway
    glActiveTexture(GL_TEXTURE0);
    texture.Bind();

    glBindVertexArray(this->quad_VAO);
    glDrawArrays(GL_TRIANGLES, 0, 6);
    glBindVertexArray(0);
}

