#include "game_object.hpp"


GameObject::GameObject()
    : position(0,0), size(1,1), velocity(0.0f), color(1.0f),
    rotation(0.0f), is_solid(false), destroyed(false), sprite() {}

GameObject::GameObject(glm::vec2 pos, glm::vec2 size, Texture2D sprite, glm::vec3 color, glm::vec2 velocity)
    : position(pos), size(size), velocity(velocity), color(color),
    rotation(0.0f), is_solid(false), destroyed(false), sprite(sprite) {}

void GameObject::Draw(SpriteRenderer& renderer)
{
    renderer.DrawSprite(this->sprite, this->position, this->size, this->rotation, this->color);
}

