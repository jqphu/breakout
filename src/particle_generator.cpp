
#include <vector>

#include "particle_generator.hpp"


ParticleGenerator::ParticleGenerator(Shader shader, Texture2D texture, GLuint amount, std::string colour)
    :amount(amount), shader(shader), texture(texture), colour(colour)
{
    this->Init();
}

void ParticleGenerator::Init()
{
    // Set up mesh and attributes
    GLuint VBO;
    // Pos, texture coords
    GLfloat particle_quad[] =
    {
        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f, 0.0f,

        0.0f, 1.0f, 0.0f, 1.0f,
        1.0f, 1.0f, 1.0f, 1.0f,
        1.0f, 0.0f, 1.0f, 0.0f
    };
    glGenVertexArrays(1, &this->VAO);
    glGenBuffers(1, &VBO);
    glBindVertexArray(this->VAO);

    // Fill the buffers
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(particle_quad), particle_quad, GL_STATIC_DRAW);
    // Set mesh attributes
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, 4 * sizeof(GLfloat), (GLvoid*)0);
    glBindVertexArray(0);

    for(GLuint i = 0; i < this->amount; ++i)
    {
        // Create an empty particle and push it back
        this->particles.push_back(Particle());
    }
}

void ParticleGenerator::Update(GLfloat dt, GameObject& object, GLuint new_particles, glm::vec2 offset)
{
    // Add new particles!
    for (GLuint i = 0; i < new_particles; ++i)
    {
        GLuint unused_particle = this->FirstUnusedParticle();
        this->RespawnParticle(this->particles[unused_particle], object, offset);
    }

    // Update all the particles positions and colors!
    for (GLuint i = 0; i < this->amount; ++i)
    {
        Particle &p = this->particles[i];
        p.life -= dt;

        // If it is still alive
        if(p.life > 0.0f)
        {
            // Update position and colour
            p.position -=- p.velocity * dt;
            p.color.a -= dt * 2.5; // Intensity decrease!
        }
    }
}

void ParticleGenerator::Draw()
{
    // New blending mode to give a glow effect when stacked
    glBlendFunc(GL_SRC_ALPHA, GL_ONE);
    this->shader.Use();
    for(Particle particle : this->particles)
    {
		if (particle.life > 0.0f)
		{
			this->shader.SetVector2f("offset", particle.position);
			this->shader.SetVector4f("color", particle.color);
			this->texture.Bind();
			glBindVertexArray(this->VAO);
			glDrawArrays(GL_TRIANGLES, 0, 6);
			glBindVertexArray(0);
		}
    }
    // Back to default blending mode
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}


GLuint ParticleGenerator::FirstUnusedParticle()
{

    // Static, lasts whole lifetime of program
    static GLuint last_used_particle = 0;
    for(GLuint i = last_used_particle; i < this->amount; ++i)
    {
        if(this->particles[i].life <= 0.0f)
        {
            last_used_particle = i;
            return i;
        }
    }

    // Can't find, search from start
    for(GLuint i = 0; i < last_used_particle; ++i)
    {
        if(this->particles[i].life <= 0.0f)
        {
            last_used_particle = i;
            return i;
        }
    }

    // Every particle is alive! Overwrite first particle
    // If we reach this, we should create more particles or
    // they are living too long
    last_used_particle = 0;
    return 0;
}

void ParticleGenerator::RespawnParticle(Particle& particle, GameObject& object, glm::vec2 offset)
{
    GLfloat random_noise = ((rand() % 100) - 50) / 10.0f;
    GLfloat random_color = 0.5 + ((rand() %100)/100.0f);
    // Make particles float around a bit
    particle.position = object.position + random_noise + offset;

    // Random brightness!
    if(this->colour == "fire")
    {
        particle.color = glm::vec4(0.88f, 0.34f, 0.13f, 1.0f);
    }
    else
    {
        particle.color = glm::vec4(random_color, random_color, random_color, 1.0f);
    }
    particle.life = 1.0f;
    particle.velocity = object.velocity * 0.1f;
}
