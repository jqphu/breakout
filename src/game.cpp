/*******************************************************************
 ** This code is part of Breakout.
 **
 ** Breakout is free software: you can redistribute it and/or modify
 ** it under the terms of the CC BY 4.0 license as published by
 ** Creative Commons, either version 4 of the License, or (at your
 ** option) any later version.
 ******************************************************************/
#include "game.hpp"
#include "resource_manager.hpp"
#include "sprite_renderer.hpp"
#include "ball_object.hpp"
#include "post_processor.hpp"

#include <algorithm>
#include <tuple>


// These are global variables that can only be accessed by GAME!
const char * const kLevelNames[] = {
    "one.lvl",
    "two.lvl",
    "three.lvl",
    "four.lvl"
};
const unsigned int kNumLevels = 4;
const unsigned int kPosPowerupChance = 75;
const unsigned int kNegPowerupChance = 15;
const unsigned int kMaxPadSize = 500;

/**
 * Create a sprite renderer
 */
SpriteRenderer *renderer;

/**
 * Players paddle
 */
const glm::vec2 kPlayerSize(100,20); // Only base value, will change with powerups
const GLfloat kPlayerVelocity(600.0f);

// Scale of the extra horizontal velocity when hidding edges of paddle
const GLfloat kScale(2.0f);

/**
 * Ball object
 */
// Inital velocity
const glm::vec2 kInitalBallVelocity(100.0f, -500.0f);

// Radius of ball object
const GLfloat kBallRadius = 12.5f;

// Stores the ball
BallObject* ball;

// Game object stores a player
// Position defined by left of paddle
GameObject *player;

// Particle generator
ParticleGenerator* particles;

// Particle generator
ParticleGenerator* particles_fire;


const unsigned int kNumParticles = 500;

// Post processor
PostProcessor* effects;
GLfloat shake_time = 0.0f;


Game::Game(GLuint width, GLuint height)
    : state(kGameActive), keys(), width(width), height(height)
{

}

Game::~Game()
{
    delete renderer;
    delete player;
    delete ball;
    delete particles;
    delete particles_fire;
    delete effects;
}

void Game::Init()
{
    // Load in shaders
    ResourceManager::LoadShader("sprite.vs", "sprite.fs", nullptr, "sprite");
    ResourceManager::LoadShader("particle.vs", "particle.fs", nullptr, "particle");
    ResourceManager::LoadShader("post_processing.vs", "post_processing.fs", nullptr, "postprocessing");

    // Configure shaders
    glm::mat4 projection = glm::ortho(0.0f, static_cast<GLfloat>(this->width),
            static_cast<GLfloat>(this->height), 0.0f, -1.0f, 1.0f);
    ResourceManager::GetShader("sprite").Use().SetInteger("image", 0);
    ResourceManager::GetShader("sprite").SetMatrix4("projection", projection);
    ResourceManager::GetShader("particle").Use().SetInteger("image", 0);
    ResourceManager::GetShader("particle").SetMatrix4("projection", projection);


    // Set render specific controls
    renderer = new SpriteRenderer(ResourceManager::GetShader("sprite"));

    // Load textures!
    ResourceManager::LoadTexture("awesomeface.jpg", GL_TRUE, "face");
    ResourceManager::LoadTexture("background.jpg", GL_TRUE, "background");
    ResourceManager::LoadTexture("block.png", GL_TRUE, "block");
    ResourceManager::LoadTexture("block_solid.png", GL_TRUE, "block_solid");
    ResourceManager::LoadTexture("paddle.png", GL_TRUE, "paddle");
    ResourceManager::LoadTexture("Ruu.png", GL_TRUE, "ball");
    ResourceManager::LoadTexture("particle_fire.png", GL_TRUE, "particle_fire");
    ResourceManager::LoadTexture("particle.png", GL_TRUE, "particle");

    // Powerups
    ResourceManager::LoadTexture("powerup_speed.png", GL_TRUE, "powerup_speed");
    ResourceManager::LoadTexture("powerup_sticky.png", GL_TRUE, "powerup_sticky");
    ResourceManager::LoadTexture("powerup_increase.png", GL_TRUE, "powerup_increase");
    ResourceManager::LoadTexture("powerup_confuse.png", GL_TRUE, "powerup_confuse");
    ResourceManager::LoadTexture("powerup_chaos.png", GL_TRUE, "powerup_chaos");
    ResourceManager::LoadTexture("powerup_passthrough.png", GL_TRUE, "powerup_passthrough");
    ResourceManager::LoadTexture("powerup_bomb.png", GL_TRUE, "powerup_bomb");

    // Create the particles
    particles = new ParticleGenerator(
            ResourceManager::GetShader("particle"),
            ResourceManager::GetTexture("particle"),
            kNumParticles);

    // Create the particles
    particles_fire = new ParticleGenerator(
            ResourceManager::GetShader("particle"),
            ResourceManager::GetTexture("particle_fire"),
            2*kNumParticles, "fire" );



    // Create post-processor
    effects = new PostProcessor(ResourceManager::GetShader("postprocessing"), this->width, this->height);

    // Load levels
    for(unsigned int i = 0; i < kNumLevels; ++i)
    {
        GameLevel cur_level;
        cur_level.Load(kLevelNames[i], this->width, this->height*0.5);
        this->levels.push_back(cur_level);
    }

    this->current_level = 1;

    // Create the player!
    // Start in the middle
    glm::vec2 player_pos = glm::vec2(
            this->width/2 - kPlayerSize.x / 2,
            this->height - kPlayerSize.y
            );

    player = new GameObject(player_pos, kPlayerSize, ResourceManager::GetTexture("paddle"));

    // Create the ball
    glm::vec2 ball_pos = player_pos + glm::vec2(kPlayerSize.x  / 2 - kBallRadius, -kBallRadius * 2);
    ball = new BallObject(ball_pos, kBallRadius, kInitalBallVelocity, ResourceManager::GetTexture("ball"));


    // Effects
    //effects->shake = GL_TRUE;
    //effects->confuse = GL_TRUE;
    //effects->chaos = GL_TRUE;


}

void Game::Update(GLfloat dt)
{
    ball->Move(dt, this->width);

    this->UpdateCollisions();

    if(ball->position.y >= this->height)
    {
        this->ResetLevel();
        this->ResetPlayer();
        this->ResetPowerup();
    }

    // Update the particle around the ball
    particles_fire->Update(dt, *ball, 2, glm::vec2(ball->radius / 2));
    particles->Update(dt, *ball, 2, glm::vec2(ball->radius / 2));

    this->UpdatePowerups(dt);

    if( shake_time > 0.0f)
    {
        shake_time -= dt;
        if(shake_time <= 0.0f)
        {
            effects->shake = false;
        }
    }
}


void Game::ProcessInput(GLfloat dt)
{
    if (this->state == kGameActive)
    {
        GLfloat velocity = kPlayerVelocity * dt;

        // Move the paddle
        if (this->keys[GLFW_KEY_A])
        {
            if(player->position.x > 0)
            {
                player->position.x = std::max(player->position.x - velocity, 0.0f);

                // Launch to the left
                if(ball->stuck)
                {
                    ball->position.x -= velocity;
                }
            }
        }
        if (this->keys[GLFW_KEY_D])
        {
            if(player->position.x < this->width - player->size.x)
            {
                player->position.x = std::min(player->position.x + velocity, this->width - player->size.x);

                // Launch to the right
                if(ball->stuck)
                {
                    ball->position.x += velocity;
                }

            }
        }

        if(this->keys[GLFW_KEY_SPACE])
        {
            ball->stuck = false;
        }

    }

}

void Game::Render()
{

    if(this->state == kGameActive)
    {
        effects->BeginRender();

        // Draw background
        renderer->DrawSprite(ResourceManager::GetTexture("background"),
                glm::vec2(0,0), glm::vec2(this->width, this->height), 0.0f
                );

        // Draw the current level
        this->levels[this->current_level].Draw(*renderer);

        // Draw player
        player->Draw(*renderer);

        // Draw particles
        if(ball->bomb)
        {
            particles_fire->Draw();
        }
        else
        {
            particles->Draw();
        }

        // Draw ball
        ball->Draw(*renderer);

        // Powerups
        for (Powerup &powerup : this->powerups)
        {
            if (!powerup.destroyed)
            {
                powerup.Draw(*renderer);
            }

        }

        effects->EndRender();

        effects->Render(glfwGetTime());
    }
}

const uint kExplodeRadius = 3; // NUmber of blocks to eXPLODE

void Game::ExplodeAroundTile(glm::vec2 pos)
{

    ball->bomb = GL_FALSE;
    // Look through it iteratively
    for(GameObject& tile : this->levels[this->current_level].bricks)
    {
        if(tile.destroyed) continue;
        if(tile.position.x < pos.x + kExplodeRadius*tile.size.x/2.0
                && tile.position.x > pos.x - kExplodeRadius*tile.size.x/2.0
                && tile.position.y < pos.y + kExplodeRadius*tile.size.y/2.0
                && tile.position.y > pos.y - kExplodeRadius*tile.size.y/2.0)
        {
            tile.destroyed = GL_TRUE;

            // Maybe spawn special powerup for destroyed solid blocks
            this->SpawnPowerups(tile);
        }
    }


}

void Game::UpdateCollisions()
{
    // Look through it iteratively
    for(GameObject& tile : this->levels[this->current_level].bricks)
    {
        if(!tile.destroyed)
        {
            Collision collision = IsCollision(*ball, tile);
            // If there is a collision
            if(std::get<0>(collision))
            {

                // Has the bomb effect!
                if(ball->bomb)
                {
                    this->ExplodeAroundTile(ball->position);

                    // Don't redirect the ball, keep it going on the same path
                    continue;

                }
                // If it not a destroyed block
                if(!tile.is_solid)
                {
                    tile.destroyed = GL_TRUE;
                    this->SpawnPowerups(tile);
                }
                else // Block is solid!
                {
                    // Shake!
                    shake_time = 0.05f;
                    effects->shake = true;
                }

                if(!(ball->pass_through) || tile.is_solid)
                {

                    Direction dir = std::get<1>(collision);
                    glm::vec2 diff_vector = std::get<2>(collision);

                    // Redirecting the ball
                    if( dir == kLeft || dir == kRight)
                    {
                        ball->velocity.x = -ball->velocity.x;

                        // Move the ball out of the block
                        GLfloat penetration = ball->radius - std::abs(diff_vector.x);
                        // If we were going left when we hit the block
                        if(dir == kLeft)
                        {
                            ball->position.x += penetration;
                        }
                        else
                        {
                            ball->position.x -= penetration;
                        }
                    }
                    else
                    {
                        ball->velocity.y = -ball->velocity.y;

                        GLfloat penetration = ball->radius - std::abs(diff_vector.x);
                        if (dir == kUp)
                        {
                            ball->position.y -= penetration;// Move ball back up
                        }
                        else
                        {
                            ball->position.y += penetration;
                        }
                    }
                }
            }
        }
    }

    // Check if collision with the paddle
    Collision player_collision = IsCollision(*ball, *player);


    // If the ball is not stuck and we have collided
    if(!ball->stuck && std::get<0>(player_collision))
    {
        GLfloat center_paddle = player->position.x + player->size.x / 2;

        // Distnace between centers
        GLfloat distance = (ball->position.x + ball->radius) - center_paddle;

        GLfloat percentage = distance / (player->size.x/2);

        // Strength
        glm::vec2 old_velocity = ball->velocity;

        ball->velocity.x = kInitalBallVelocity.x * percentage * kScale;
        ball->velocity.y = -ball->velocity.y;
        // Quickfix for ball getting stuck in middle of paddle!
        ball->velocity.y = -1 * abs(ball->velocity.y);


        // Same overall speed! just different direction
        ball->velocity = glm::normalize(ball->velocity) * glm::length(old_velocity);

        ball->stuck = ball->sticky;
    }

    // Update possible powerup
    for(Powerup &powerup : this->powerups)
    {
        if(!powerup.destroyed)
        {
            // Reached bottom of screen
            if (powerup.position.y >= this->height)
            {
                powerup.destroyed = GL_TRUE;
            }
        }

        // Collides with player
        if (IsCollision(*player, powerup))
        {   // Collided with player, now activate powerup
            ActivatePowerup(powerup);
            powerup.destroyed = GL_TRUE;
            powerup.activated = GL_TRUE;
        }
    }
}


void Game::ResetPowerup()
{
    this->UpdatePowerups(1000);
    this->powerups.clear();

}
void Game::ResetLevel()
{
    this->levels[this->current_level].Load(kLevelNames[this->current_level],
            this->width, this->height * 0.5);
}

void Game::ResetPlayer()
{
    player->size = kPlayerSize;
    player->position = glm::vec2(this->width / 2 - kPlayerSize.x, this->height - kPlayerSize.y);
    ball->Reset(player->position + glm::vec2(kPlayerSize.x / 2 - kBallRadius, -(kBallRadius * 2)), kInitalBallVelocity);
}


/**
 * Rectangle colission
 */
GLboolean IsCollision(GameObject& one, GameObject& two)
{
    // Possible x collision
    GLboolean collision_x = one.position.x + one.size.x >= two.position.x &&
        two.position.x + two.size.x  >=  one.position.x;
    GLboolean collision_y  =  one.position.y + one.size.y >= two.position.y &&
        two.position.y + two.size.y >= one.position.y;

    return collision_x && collision_y;
}

/**
 * Circle collision
 * Returns what direction to be going next
 */
Collision IsCollision(BallObject& ball, GameObject& tile)
{
    // Get the center
    // Recall one.position is the top left corner
    glm::vec2 center(ball.position + ball.radius);

    //Calculate AABB info
    glm::vec2 aabb_half_extents(tile.size.x / 2, tile.size.y / 2);
    glm::vec2 aabb_center(
            tile.position.x + aabb_half_extents.x,
            tile.position.y + aabb_half_extents.y
            );
    // Difference vector between both centers
    glm::vec2 difference = center - aabb_center;
    glm::vec2 clamped = glm::clamp(difference, -aabb_half_extents, aabb_half_extents);
    // Add clamped value to AABB_center and we get the value of box closest to circle
    glm::vec2 closest = aabb_center + clamped;
    // Retrieve vector between center circle and closest point AABB and check if length <= radius
    difference = closest - center;
    if (glm::length(difference) <= ball.radius)
    {
        return std::make_tuple(GL_TRUE, VectorDirection(difference), difference);
    }
    else
    {
        return std::make_tuple(GL_FALSE, kUp, glm::vec2(0, 0));
    }
}


Direction VectorDirection(glm::vec2 target)
{
    glm::vec2 compass[] =
    {
        glm::vec2(0.0f, 1.0f),  // up
        glm::vec2(1.0f, 0.0f),  // right
        glm::vec2(0.0f, -1.0f), // down
        glm::vec2(-1.0f, 0.0f)  // left
    };

    GLfloat max = 0.0f;
    GLuint best_match = -1;

    for (GLuint i = 0; i < 4; i++)
    {
        GLfloat dot_product = glm::dot(glm::normalize(target), compass[i]);
        if (dot_product > max)
        {
            max = dot_product;
            best_match = i;
        }
    }

    return (Direction)best_match;
}


GLboolean ShouldSpawn(GLuint chance)
{
    return (rand() % chance) == 0;
}

void Game::SpawnPowerups(GameObject& block)
{
    if (ShouldSpawn(kPosPowerupChance))
    {
        this->powerups.push_back(
                Powerup("speed", glm::vec3(0.5f, 0.5f, 1.0f), 0.0f, block.position, ResourceManager::GetTexture("powerup_speed")));
    }
    else if (ShouldSpawn(kPosPowerupChance))
    {
        this->powerups.push_back(
                Powerup("sticky", glm::vec3(1.0f, 0.5f, 1.0f), 10.0f, block.position, ResourceManager::GetTexture("powerup_sticky")));
    }

    else if (ShouldSpawn(kPosPowerupChance))
    {
        this->powerups.push_back(
                Powerup("pass-through", glm::vec3(0.5f, 1.0f, 0.5f), 5.0f, block.position, ResourceManager::GetTexture("powerup_passthrough")));
    }

    else if (ShouldSpawn(kPosPowerupChance))
    {
        this->powerups.push_back(
                Powerup("pad-size-increase", glm::vec3(1.0f,0.6f,0.4f), 0.0f, block.position,  ResourceManager::GetTexture("powerup_increase")));
    }

    else if (ShouldSpawn(kPosPowerupChance))
    {
        this->powerups.push_back(
                Powerup("bomb", glm::vec3(1.0f,0.6f,0.0f), 0.0f, block.position,  ResourceManager::GetTexture("powerup_bomb")));
    }
    else if (ShouldSpawn(kNegPowerupChance))
    {
        this->powerups.push_back(
                Powerup("confuse", glm::vec3(1.0f, 0.3f, 0.3f), 7.5f, block.position,  ResourceManager::GetTexture("powerup_confuse")));
    }


    else if (ShouldSpawn(kNegPowerupChance))
    {
        this->powerups.push_back(
                Powerup("chaos", glm::vec3(0.9f, 0.25f, 0.25f), 5.0f, block.position,ResourceManager::GetTexture("powerup_chaos")));
    }
}
void ActivatePowerup(Powerup &powerup)
{
    // Initiate a powerup based type of powerup
    if (powerup.type == "speed")
    {
        ball->velocity *= 1.2;
    }
    else if (powerup.type == "sticky")
    {
        ball->sticky = GL_TRUE;
        player->color = glm::vec3(1.0f, 0.5f, 1.0f);
    }
    else if (powerup.type == "pass-through")
    {
        ball->pass_through = GL_TRUE;
        ball->color = glm::vec3(1.0f, 0.5f, 0.5f);
    }
    else if (powerup.type == "pad-size-increase")
    {
        player->size.x = std::min(player->size.x + 50, (float)kMaxPadSize);
    }
    else if (powerup.type == "confuse")
    {
        if (!effects->chaos)
            effects->confuse = GL_TRUE; // Only activate if chaos wasn't already active
    }
    else if (powerup.type == "chaos")
    {
        if (!effects->confuse)
            effects->chaos = GL_TRUE;
    }
    else if (powerup.type == "bomb")
    {
        ball->bomb = GL_TRUE;
        ball->color = glm::vec3(0.5f, 0.0f, 0.0f);
    }

}

void Game::UpdatePowerups(GLfloat dt)
{
    for (Powerup &powerup : this->powerups)
    {
        powerup.position += powerup.velocity * dt;
        if (powerup.activated)
        {
            powerup.duration -= dt;

            if (powerup.duration <= 0.0f)
            {
                // Remove powerup from list (will later be removed)
                powerup.activated = GL_FALSE;
                // Deactivate effects
                if (powerup.type == "sticky")
                {
                    if (!IsOtherPowerupActive(this->powerups, "sticky"))
                    {	// Only reset if no other Powerup of type sticky is active
                        ball->sticky = GL_FALSE;
                        player->color = glm::vec3(1.0f);
                    }
                }
                else if (powerup.type == "pass-through")
                {
                    if (!IsOtherPowerupActive(this->powerups, "pass-through"))
                    {	// Only reset if no other Powerup of type pass-through is active
                        ball->pass_through = GL_FALSE;
                        ball->color = glm::vec3(1.0f);
                    }
                }
                else if (powerup.type == "bomb")
                {
                    if (!ball->bomb)
                    {
                        ball->color = glm::vec3(1.0f);
                    }
                    else
                    {
                        powerup.activated = GL_TRUE;
                    }
                }

                else if (powerup.type == "confuse")
                {
                    if (!IsOtherPowerupActive(this->powerups, "confuse"))
                    {	// Only reset if no other Powerup of type confuse is active
                        effects->confuse = GL_FALSE;
                    }
                }
                else if (powerup.type == "chaos")
                {
                    if (!IsOtherPowerupActive(this->powerups, "chaos"))
                    {	// Only reset if no other Powerup of type chaos is active
                        effects->chaos = GL_FALSE;
                    }
                }
            }
        }
    }
    this->powerups.erase(std::remove_if(this->powerups.begin(), this->powerups.end(),
        [](const Powerup &powerup) { return powerup.destroyed && !powerup.activated; }
    ), this->powerups.end());
}

GLboolean IsOtherPowerupActive(std::vector<Powerup> &powerups, std::string type)
{
    for (const Powerup &powerup : powerups)
    {
        if (powerup.activated)
            if (powerup.type == type)
                return GL_TRUE;
    }
    return GL_FALSE;
}
