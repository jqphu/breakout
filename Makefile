#
# TODO: Move `libmongoclient.a` to /usr/local/lib so this can work on production servers
#

CC := g++ # This is the main compiler
# CC := clang --analyze # and comment out the linker last line for sanity
SRCDIR := src
OBJDIR := obj
TARGET := bin/breakout

# Grab every file that satisfies *.c**  in the source directory
# [^.] to ignore hidden files
SOURCES := $(shell find $(SRCDIR) -type f -name [^.]*.c**)


# $(basename $(SOURCES)) grabs all the files in SOURCES and remove the extension (no .c, or .cpp)
# $(patsubst a, b, c) replaces all b's inside string c with a. In this case simply replacing src with obj
# $(addsuffix) adds a .o to the end!
OBJECTS := $(addsuffix .o, $(patsubst $(SRCDIR)/%,$(OBJDIR)/%,$(basename $(SOURCES))))
CFLAGS := -g -Wall -std=c++11

# Linking flags
LIB := -lGL -lGLU -lglfw3 -lX11 -lXxf86vm -lXrandr -lpthread -lXi -ldl -lXinerama -lXcursor -lSOIL
INC := -Iinclude

# To create target, need all the objects!
$(TARGET): $(OBJECTS)
	@echo " Linking..."
	@echo " $(CC) $^  -o $(TARGET) $(LIB)"; $(CC) $^ -o $(TARGET) $(LIB)


# Rules to create these object files, need each file with .c*
$(OBJDIR)/%.o: $(SRCDIR)/%.c*
	@mkdir -p $(OBJDIR)
	@echo " $(CC) $(CFLAGS) $(INC) -c -o $@ $<"; $(CC) $(CFLAGS) $(INC) -c -o $@ $<
	@echo "OBJECTS $(OBJECTS)"


clean:
	@echo " Cleaning...";
	@echo " $(RM) -r $(OBJDIR) $(TARGET)"; $(RM) -r $(OBJDIR) $(TARGET)

.PHONY: clean
